import os


def check_win(secret_word, old_letters_guessed):
    """
    checks if the player won.
    :param secret_word: the word the player needs to find
    :param old_letters_guessed: the letters the player already guessed
    :type secret_word: str
    :type old_letters_guessed: list
    :return: if the player won
    :rtype: bool
    """
    for letter in secret_word:
        if letter not in old_letters_guessed:
            return False
    return True


def show_hidden_word(secret_word, old_letters_guessed):
    """
    get the secret word's hidden format to later display to the player
    :param secret_word: the word the player needs to find
    :param old_letters_guessed: the letters the player already guessed
    :type secret_word: str
    :type old_letters_guessed: list
    :return: hidden word with the player's right guess on it
    :rtype: str
    """
    word = ''
    for letter in secret_word:
        if letter in old_letters_guessed:
            word += letter
        else:
            word += ' _ '
    return word


def check_valid_input(letter_guessed, old_letters_guessed):
    """
    checks if the player's input was valid or not.
    :param letter_guessed: the letter the player guessed
    :param old_letters_guessed: the letters the player already guessed
    :type letter_guessed: str
    :type old_letters_guessed: list
    :return: whether the input is valid
    :rtype: bool
    """
    if len(letter_guessed) == 1 and letter_guessed.isalpha() and \
            letter_guessed.lower() not in old_letters_guessed:
        return True
    return False


def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    """
    checks the players input and add it to the players guess list if needed.
    :param letter_guessed: the letter the player guessed
    :param old_letters_guessed: the letters the player already guessed
    :type letter_guessed: str
    :type old_letters_guessed: list
    :return: whether the input is valid
    :rtype: bool
    """
    if check_valid_input(letter_guessed, old_letters_guessed):
        old_letters_guessed += letter_guessed.lower()
        return True
    print("X")
    separator = " -> "
    print(separator.join(old_letters_guessed))
    return False


def choose_word(file_path, index):
    """
    chooses a word from a text file.
    :param file_path:
    :param index:
    :type file_path: str
    :type index: int
    :return: a word
    :rtype: str
    """
    with open(file_path, "r") as file:
        word_list = file.readlines()[0].split(" ")
        while not index < len(word_list):
            index = index - len(word_list)
        return word_list[index]


def clear():
    """
    clears the screen, compatible with multiple os
    """
    if os.name in ('nt', 'dos'):
        os.system('cls')
    elif os.name in ('linux', 'osx', 'posix'):
        os.system('clear')
    else:
        print("\n" * 120)


MAX_TRIES = 6
HANGMAN_ASCII_ART = \
    "    _    _\n   | |  | |\n   | |__| | __ _ _ __   __ _ _ __ ___   __ _ _" \
    " __\n   |  __  |/ _' | '_ \\ / _' | '_ ' _ \\ / _' | '_ \\\n   | |  | |" \
    " (_| | | | | (_| | | | | | | (_| | | | |\n   |_|  |_|\\__,_|_| |_|\\__," \
    " |_| |_| |_|\\__,_|_| |_|\n                        __/ |\n             " \
    "          |___/\n "

HANGMAN_PHOTOS = {0: "",
                  1: "    x-------x",
                  2: "    x-------x\n    |\n    |\n    |\n    |\n    |\n",
                  3: "x-------x\n    |       |\n    |       0\n    |\n    "
                     "|\n    |\n",
                  4: "x-------x\n    |       |\n    |       0\n    |       "
                     "|\n    |\n    |\n",
                  5: "x-------x\n    |       |\n    |       0\n    |      "
                     "/|\\\n    |\n    |\n",
                  6: "x-------x\n    |       |\n    |       0\n    |      "
                     "/|\\\n    |      /\n    |\n",
                  7: "x-------x\n    |       |\n    |       0\n    |      "
                     "/|\\\n    |      / \\\n    |\n"}


def print_screen(num_of_tries, str1):
    """
    prints the welcome message.
    """
    clear()
    print(HANGMAN_ASCII_ART)
    print(MAX_TRIES - num_of_tries)
    print(str1)
    print(HANGMAN_PHOTOS[num_of_tries])


def main():
    """
    the main program, runs everything in the right order.
    """
    str1 = ''
    print_screen(0, str1)
    print()
    old_letters_guessed = []
    secret_word = choose_word(input("enter file path:"),
                              int(input("enter index:")))
    num_of_tries = 0
    while True:
        if check_win(secret_word, old_letters_guessed):
            str1 = show_hidden_word(secret_word, old_letters_guessed)
            print_screen(num_of_tries, str1)
            print("congrats, you won!!!")
            break
        str2 = str1
        str1 = show_hidden_word(secret_word, old_letters_guessed)
        if str1 == str2:
            num_of_tries += 1
        print_screen(num_of_tries, str1)
        guess = input("guess a letter:")
        while not try_update_letter_guessed(guess, old_letters_guessed):
            guess = input("guess a letter:")
        if num_of_tries == MAX_TRIES:
            print_screen(7, str1)
            print("you lost :(")
            break


if __name__ == '__main__':
    main()
